import React, { Component } from 'react';
import logo from './logo.svg';

class Create extends Component {
  render() {
    return (
      <div>
        <h4 id="header">Presensi KM ITB</h4>
        <h6>Tambah Rapat</h6>
        <form>
          <label>Nama Rapat</label>
          <input type="text" className="form-control" />
          <label>Tanggal Rapat</label>
          <input type="date" className="form-control" />
          <label>Waktu Rapat</label>
          <input type="time" className="form-control" />
          <input type="submit" value="Tambahkan" className="form-control" />
        </form>
        <h6>Daftar Rapat</h6>
        <table id="myTable4" className="display table table-hover">
        <thead>
            <tr>
                <th>No</th>
                <th>Rapat</th>
                <th>Tanggal</th>
                <th>Waktu</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
          <tr>
              <td>1</td>
              <td>asd</td>
              <td>asd</td>
              <td>asd</td>
              <td>
                <form>
                  <input type="submit" name="" value="Hapus" className="btn btn-default" />
                </form>
              </td>
          </tr>
        </tbody>
      </table>
      </div>
    );
  }
}

export default Create;
