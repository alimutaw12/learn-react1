import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nama : '',
      currentRapat: 'Rabu - 12 Januari 2018',
      currentLembaga: 'Himpunan',
      optionRapat: [{hari:'Rabu', tanggal:'12 Januari 2018'}, {hari:'Rabu', tanggal:'19 Januari 2018'}],
      optionLembaga: ['Himpunan', 'Unit', 'KM ITB']
  };

    this.handleRapatChange = this.handleRapatChange.bind(this);
    this.handleLembagaChange = this.handleLembagaChange.bind(this);
  }

  handleRapatChange(event) {
    let value = event.target.value;
    this.setState({currentRapat: value});
    // if (this.state.currentRapat == 'Rabu - 12 Januari 2018') {
    //   this.setState({nama: 'Ali'});
    // } else {
    //   this.setState({nama: 'Mutawalli'});
    // }
  }

  handleLembagaChange(event) {
    this.setState({currentLembaga: event.target.value});
    console.log(this.state.currentLembaga);
  }

  render() {
    return (
      <div>
        <h4 id="header">Belajar React</h4>
        <a href="" className="btn btn-default form-control">Tambah</a>
        <form>
        <label>NIM :</label>
        <input type="text" className="form-control" />
        <input type="submit" className="form-control btn btn-default" />
        </form>
        <form>
        <label>Nama</label>
        <input type="text" value={this.state.nama} className="form-control" disabled />
          <label>Rapat</label>
          <select onChange={this.handleRapatChange} className="form-control">
            {this.state.optionRapat.map( (rapat, idx) => {
              let nilai = `${rapat.hari} - ${rapat.tanggal}`
              return <option key={idx} value={nilai}>{nilai}</option>
            })}
          </select>
          <label>Lembaga</label>
          <select onChange={this.handleLembagaChange} className="form-control">
            {this.state.optionLembaga.map( (lembaga, idx) => {
              return <option key={idx} value={lembaga}>{lembaga}</option>
            })}
          </select>
        </form>
        <div className="row">
          <div className="col-md-6">
            <button className="btn btn-default form-control" disabled>Check in</button>
          </div>
          <div className="col-md-6">
            <button className="btn btn-default form-control" disabled>Check out</button>
          </div>
        </div>
        <a href="" className="btn btn-default form-control">View Absen</a>
      </div>
    );
  }
}

export default App;
